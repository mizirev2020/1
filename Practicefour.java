package com.company;

import java.util.Scanner;

public class Practicefour {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите текст");
        String text = in.nextLine();
        if (text.length() > 0) {
            reverseString(text, text.length() - 1);
        }
    }
    public static void reverseString(String text, int index) {
        if(index == 0) {
            System.out.println(text.charAt(index));

            return;
        }

        System.out.print(text.charAt(index));

        reverseString(text, index - 1);
    }
}
